//
//  ViewController.m
//  PageControl
//
//  Created by 網際優勢(股)公司Linda Lin on 2015/4/15.
//  Copyright (c) 2015年 uxb2b. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pageChanged:(UIPageControl*)sender {
    self.myImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"dog%ld.jpg", sender.currentPage+1]];
}
@end
